#!/bin/bash

function print_spaces {
  SP="\ "
  eval "printf "$SP"%.0s {1..$1}"
}

function print_mins {
  eval "printf -- "-"%.0s {1..$1}"
}

function pipes_line {
  SS="$1"
  for ((k=0; k < TOREP; ++k))
  do
    print_spaces $SS
    printf $2
    print_spaces $((2*SS+1))
    printf $2
    print_spaces $((SS + 1))
  done  
  echo ""
}

function min_line {
  for ((k=0; k < TOREP; ++k))
  do
    print_spaces $SN
    printf "+"
    print_mins $SN
    printf "o"
    print_mins $SN
    printf "+"
    print_spaces $((SN+1))
  done  
  echo ""
}

LEVELS="$1"

print_spaces `echo "2^$((LEVELS))-2" | bc`
printf "|\n"
for ((i=LEVELS-1; i >= 1; --i))
do
  TOBC="2^$i - 1"
  SN=`echo $TOBC | bc`
  TOBC="2^$((LEVELS - i - 1))"
  TOREP=`echo $TOBC | bc`
 echo "`min_line`" | sed -e '1s/^.//' | cat 
  echo "`pipes_line $SN "|"`" | sed -e '1s/^.//' | cat 
done

echo "`pipes_line $SN "o"`" | sed -e '1s/^.//' | cat 
