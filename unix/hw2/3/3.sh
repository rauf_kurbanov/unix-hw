#!/bin/bash

printf ".\n\n"
DEPTH="1"
while [ -n "`find -mindepth $DEPTH -maxdepth $DEPTH `" ]
do
  find -mindepth $DEPTH -maxdepth $DEPTH | egrep -o "[^/]*$" | sort
  echo ""
  ((++DEPTH))
done
