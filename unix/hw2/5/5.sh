#!/bin/bash

## How to use
## Add into crontab -e 
## * * * * * /path/to/this/script

KUZID=347745
URL="https://api.vk.com/method/users.get?user_id=$KUZID&v=5.37&fields=online"

if [ ! -e /tmp/kuzprev ]
then
    echo 0 > /tmp/kuzprev
fi

PREV=$(cat /tmp/kuzprev)

CUR=`curl -s $URL |  grep -o -E 'online":[^ ]' | sed 's/online"://'`
if [[ ( $PREV != $CUR ) ]]; then 
  PREV=$CUR
  if [[ ( $CUR == 1 ) ]]; then 
    echo "KUZNETSOV IN" | wall 
  else
    echo "KUZNETSOV OUT" | wall
  fi
fi

echo $PREV > /tmp/kuzprev