#!/bin/bash

read line

if (( "$line" <= 1 ))
then
  echo "$line"
fi

PP=0
P=1
for i in $(seq 2 "$line")
do
  TMP=$PP
  PP=$P
  P=`echo "$TMP + $PP" | bc`
done

echo $P | sed 's/\\ //g'
